package com.example.pokedex

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SegmentedButtonDefaults.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.example.pokedex.pokemonDetail.PokemonDetail
import com.example.pokedex.pokemonDetail.PokemonDetailViewModel
import com.example.pokedex.pokemonsFavouritesList.PokemonsFavouritesList
import com.example.pokedex.pokemonsFavouritesList.PokemonsFavouritesListViewModel
import com.example.pokedex.pokemonsList.PokemonsList
import com.example.pokedex.pokemonsList.PokemonsListViewModel
import com.example.pokedex.ui.theme.PokedexTheme
import com.example.pokedex.ui.theme.Red

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        val context = applicationContext
        val pokemonsRoute = getString(R.string.pokemons_route)
        val pokemonDetailRoute = getString(R.string.pokemon_detail_route)
        val pokemonArgument = getString(R.string.pokemon_argument)
        val pokemonsFavouritesRoute = "favourites"

        PokemonsList.viewModel = PokemonsListViewModel()
        PokemonDetail.viewModel = PokemonDetailViewModel()
        PokemonsFavouritesList.viewModel = PokemonsFavouritesListViewModel()
        PokemonsList.routeBase = pokemonsRoute
        PokemonDetail.routeBase = "$pokemonDetailRoute?${pokemonArgument}="
        PokemonsFavouritesList.routeBase = pokemonsFavouritesRoute
        PokemonDetail.route = "$pokemonDetailRoute?${pokemonArgument}={$pokemonArgument}"
        PokemonDetail.argument = pokemonArgument

        super.onCreate(savedInstanceState)
        setContent {
            PokedexTheme {
                Surface(
                    color = MaterialTheme.colorScheme.background,
                    modifier = Modifier.fillMaxSize()
                ) {
                    Scaffold(
                        topBar = {
                            TopAppBar(
                                colors = TopAppBarDefaults.topAppBarColors(
                                    containerColor = Red,
                                    titleContentColor = MaterialTheme.colorScheme.primary,
                                ),
                                title = {
                                    Text("Pokedex", color = MaterialTheme.colorScheme.onPrimary)
                                },
                                navigationIcon = {
                                    val logoPainter =
                                        rememberImagePainter("https://www.pikpng.com/pngl/b/59-590092_pokeball-download-transparent-png-image-png-download-clipart.png")
                                    Image(
                                        painter = logoPainter,
                                        contentDescription = R.string.app_name.toString(),
                                        modifier = Modifier.size(60.dp),
                                        contentScale = ContentScale.Fit
                                    )
                                },
//                                actions = {
//                                    FavouriteButton(navController = navController)
//                                }
                            )
                        }
                    ) { innerPadding ->
                        Surface(modifier = Modifier.padding(innerPadding)) {
                            MainNavGraph(context)
                        }
                    }
                }
            }
        }
    }
    @Composable
    fun FavouriteButton(navController: NavController) {
        IconButton(
            onClick = { navController.navigate(PokemonsFavouritesList.routeBase) },
            modifier = Modifier.clickable { navController.navigate(PokemonsFavouritesList.routeBase) }
        ) {
            Text(text = "Favourites")
        }
    }
}
