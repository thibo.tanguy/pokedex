package com.example.pokedex.retrofit

import com.example.pokedex.services.PokemonApiService

object ApiClient {
    val pokemonApiService: PokemonApiService by lazy {
        RetrofitClient.retrofit.create(PokemonApiService::class.java)
    }
}