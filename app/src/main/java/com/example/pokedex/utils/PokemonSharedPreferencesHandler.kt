package com.example.pokedex.utils

import android.content.Context
import com.example.pokedex.models.domain.Pokemon
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

object PokemonSharedPreferencesHandler {
    private const val POKEMON_LIST_KEY = "pokemon_list"

    fun savePokemon(context: Context, pokemon: Pokemon) {
        val json = Json.encodeToString(pokemon)
        val sharedPreferences = context.getSharedPreferences("PokemonSharedPreferences", Context.MODE_PRIVATE)
        sharedPreferences.edit().putString(POKEMON_LIST_KEY, json).apply()
    }

    fun loadPokemonsList(context: Context): List<Pokemon> {
        val sharedPreferences = context.getSharedPreferences("PokemonSharedPreferences", Context.MODE_PRIVATE)
        val json = sharedPreferences.getString(POKEMON_LIST_KEY, null)

        return if (json != null) {
            Json.decodeFromString<List<Pokemon>>(json)
        }

        else {
            emptyList()
        }
    }
}