package com.example.pokedex.pokemonsFavouritesList

import android.content.Context
import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex.models.domain.Pokemon
import com.example.pokedex.utils.PokemonSharedPreferencesHandler
import kotlinx.coroutines.launch

class PokemonsFavouritesListViewModel: ViewModel() {
    private val _pokemons = mutableStateOf<List<Pokemon>>(emptyList())
    private val _isLoading = mutableStateOf(false)
    val pokemons: State<List<Pokemon>> = _pokemons
    val isLoading: State<Boolean> = _isLoading

    fun loadPokemons(context: Context) {
        _isLoading.value = true
        viewModelScope.launch {
            try {
                val loadedPokemons = PokemonSharedPreferencesHandler.loadPokemonsList(context)
                _pokemons.value = loadedPokemons
                _isLoading.value = false
            }

            catch (e: Exception) {
                Log.e("error", "Request error : ", e)
                _isLoading.value = false
            }
        }
    }
}