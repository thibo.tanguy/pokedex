package com.example.pokedex.pokemonsFavouritesList

import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.pokedex.pokemonDetail.PokemonDetail
import com.example.pokedex.pokemonsList.PokemonItem
import com.example.pokedex.pokemonsList.PokemonsListViewModel
import com.example.pokedex.ui.theme.Red

object PokemonsFavouritesList {
    lateinit var viewModel: PokemonsFavouritesListViewModel
    lateinit var routeBase: String

    fun NavHostController.navigateToPokemonsFavourites(pokemonId: NavBackStackEntry) {
        navigate("${PokemonDetail.routeBase}$pokemonId")
    }

//    fun NavGraphBuilder.pokemonsFavouritesNavigationEntry(
//        navigateBack: () -> Unit,
//        viewModel: PokemonsFavouritesListViewModel
//    ) {
//        composable(
//            routeBase,
//            arguments = listOf(
//                navArgument(PokemonDetail.argument) {
//                    type = NavType.IntType
//                },
//            )
//        ) {
//            val args: Bundle = it.arguments ?: throw IllegalArgumentException()
//            val pokemonId = args.getInt(PokemonDetail.argument)
//
//            PokemonsFavouritesList.Screen(
//                navigateToPokemonDetail = navigateBack,
//                viewModel = viewModel
//            )
//        }
//    }
//
//    @Composable
//    fun Screen(
//        navigateToPokemonDetail: () -> Unit,
//        viewModel: PokemonsListViewModel
//    ) {
//        val pokemons = viewModel.pokemons.value
//        val isLoading = viewModel.isLoading.value
//        var searchText by remember { mutableStateOf("") }
//
//        if (isLoading) {
//            Box(
//                modifier = Modifier
//                    .fillMaxSize()
//                    .background(Color.White),
//                contentAlignment = Alignment.Center
//            ) {
//                CircularProgressIndicator(
//                    color = Red
//                )
//            }
//        }
//
//        else {
//            Column(modifier = Modifier.fillMaxSize()) {
//                TextField(
//                    value = searchText,
//                    onValueChange = { searchText = it },
//                    modifier = Modifier
//                        .fillMaxWidth()
//                        .padding(16.dp),
//                    label = { Text("Rechercher un pokemon ...") }
//                )
//
//                LazyColumn {
//                    val filteredPokemons = pokemons.filter {
//                        it.name.contains(searchText, ignoreCase = true)
//                    }
//                    items(filteredPokemons.size) { index ->
//                        PokemonItem(
//                            pokemon = filteredPokemons[index],
//                            navigateToPokemonDetail = navigateToPokemonDetail
//                        )
//                    }
//                }
//            }
//        }
//
//        LazyColumn {
//            items(pokemons.size) { index ->
//                PokemonItem(pokemons[index], navigateToPokemonDetail)
//            }
//        }
//    }
}
