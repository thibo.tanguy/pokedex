package com.example.pokedex.models.api

import com.example.pokedex.models.domain.Resistance

data class ApiResistanceAPI(
    val name: String,
    val damage_multiplier: Double,
    val damage_relation: String
) {
    fun toDomain(): Resistance {
        return Resistance(
            name = name,
            damageMultiplier = damage_multiplier,
            damageRelation = damage_relation
        )
    }
}