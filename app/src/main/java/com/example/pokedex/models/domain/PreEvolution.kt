package com.example.pokedex.models.domain

data class PreEvolution(
    val pokedexId: Int,
    val name: String
)