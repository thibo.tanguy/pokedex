package com.example.pokedex.models.api

import com.example.pokedex.models.domain.Pokemon

data class PokemonAPI(
    val id: Int,
    val pokedexId: Int,
    val name: String,
    val image: String,
    val sprite: String?,
    val slug: String?,
    val stats: StatsAPI,
    val apiTypes: List<TypeAPI>,
    val apiGeneration: Int,
    val apiResistances: List<ApiResistanceAPI>,
    val apiEvolutions: List<ApiEvolutionAPI>?,
    val apiResistancesWithAbilities: List<ApiResistanceAPI>?
    //    val apiPreEvolution: PreEvolution?,
) {
    fun toDomain(): Pokemon {
        return Pokemon(
            id = id,
            pokedexId = pokedexId,
            name = name,
            image = image,
            sprite = sprite,
            slug = slug,
            stats = stats.toDomain(),
            types = apiTypes.map { it.toDomain() },
            generations = apiGeneration,
            resistances = apiResistances.map { it.toDomain() },
            evolutions = apiEvolutions?.map { it.toDomain() },
            resistancesWithAbilities = apiResistancesWithAbilities?.map { it.toDomain() }
        )
    }
}