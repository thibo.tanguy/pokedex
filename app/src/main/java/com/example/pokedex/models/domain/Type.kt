package com.example.pokedex.models.domain

data class Type(
    val name: String,
    val image: String
)