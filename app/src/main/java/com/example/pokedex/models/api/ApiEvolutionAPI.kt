package com.example.pokedex.models.api

import com.example.pokedex.models.domain.Evolution

data class ApiEvolutionAPI(
    val name: String,
    val pokedexId: Int
) {
    fun toDomain(): Evolution {
        return Evolution(
            name = name,
            pokedexId = pokedexId
        )
    }
}