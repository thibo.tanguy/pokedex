package com.example.pokedex.models.api

import com.example.pokedex.models.domain.PreEvolution

data class PreEvolutionAPI(
    val pokedexIdd: Int,
    val name: String
) {
    fun toDomain(): PreEvolution {
        return PreEvolution(
            pokedexId = pokedexIdd,
            name = name
        )
    }
}