package com.example.pokedex.models.domain

data class Evolution(
    val name: String,
    val pokedexId: Int
)