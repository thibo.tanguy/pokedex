package com.example.pokedex.models.domain

data class Resistance(
    val name: String,
    val damageMultiplier: Double,
    val damageRelation: String
)