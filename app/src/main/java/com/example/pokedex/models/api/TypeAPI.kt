package com.example.pokedex.models.api

import com.example.pokedex.models.domain.Type

data class TypeAPI(
    val name: String,
    val image: String
) {
    fun toDomain(): Type {
        return Type(
            name = name,
            image = image
        )
    }
}