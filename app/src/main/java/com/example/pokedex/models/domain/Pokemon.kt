package com.example.pokedex.models.domain

import kotlinx.serialization.Serializable

@Serializable
data class Pokemon(
    val id: Int,
    val pokedexId: Int,
    val name: String,
    val image: String,
    val sprite: String?,
    val slug: String?,
    val generations: Int,
    val stats: Stats,
    val types: List<Type>,
    val resistances: List<Resistance>,
    val evolutions: List<Evolution>?,
    val resistancesWithAbilities: List<Resistance>?
    //    val apiPreEvolution: PreEvolution?,
)