package com.example.pokedex.models.api

import com.example.pokedex.models.domain.Stats

data class StatsAPI(
    val HP: Int,
    val attack: Int,
    val defense: Int,
    val special_attack: Int,
    val special_defense: Int,
    val speed: Int
) {
    fun toDomain(): Stats {
        return Stats(
            hp = HP,
            attack = attack,
            defense = defense,
            specialAttack = special_attack,
            specialDefense = special_defense,
            speed = speed
        )
    }
}