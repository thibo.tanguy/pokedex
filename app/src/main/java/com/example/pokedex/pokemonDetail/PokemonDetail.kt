package com.example.pokedex.pokemonDetail

import android.os.Bundle
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import coil.compose.rememberImagePainter
import com.example.pokedex.models.domain.Pokemon
import com.example.pokedex.models.domain.Resistance
import com.example.pokedex.models.domain.Stats
import com.example.pokedex.models.domain.Type
import com.example.pokedex.pokemonsList.PokemonThemedButton
import com.example.pokedex.ui.theme.Red
import com.example.pokedex.ui.theme.getTypeColor

object PokemonDetail {
    lateinit var viewModel: PokemonDetailViewModel
    lateinit var route: String
    lateinit var routeBase: String
    lateinit var argument: String

    fun NavHostController.navigateToPokemonDetail(pokemonId: Int) {
        navigate("$routeBase$pokemonId")
    }

    fun NavGraphBuilder.pokemonDetailNavigationEntry(
        navigateBack: () -> Unit,
        onFavouriteClick: () -> Unit,
        viewModel: PokemonDetailViewModel
    ) {
        composable(
            this@PokemonDetail.route,
            arguments = listOf(
                navArgument(argument) {
                    type = NavType.IntType
                },
            )
        ) {
            val args: Bundle = it.arguments ?: throw IllegalArgumentException()
            val pokemonId = args.getInt(argument)
            Screen(
                pokemonId = pokemonId,
                navigateBack = navigateBack,
                onFavouriteClick = onFavouriteClick,
                viewModel = viewModel
            )
        }
    }
}

@Composable
private fun Screen(
    pokemonId: Int,
    navigateBack: () -> Unit,
    onFavouriteClick: () -> Unit,
    viewModel: PokemonDetailViewModel
) {
    val pokemonState by remember { viewModel.pokemon }
    viewModel.loadPokemonById(pokemonId)
    val isLoading = viewModel.isLoading.value
    val requestFailed = viewModel.requestFailed.value

    if (isLoading) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator(
                color = Red
            )
        }
    } else {
        if (requestFailed) {
            PokemonThemedButton(
                onClick = { viewModel.loadPokemonById(pokemonId) },
            )
        }

        pokemonState?.let { pokemon ->
            PokemonItem(pokemon, navigateBack, onFavouriteClick)
        }
    }
}

@Composable
fun PokemonItem(pokemon: Pokemon, navigateBack: () -> Unit, onFavoriteClick: () -> Unit) {
    val backgroundColor = getTypeColor(pokemon.types.last().name)

    Card(
        shape = RoundedCornerShape(16.dp),
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
    ) {
        Surface(
            color = backgroundColor,
            modifier = Modifier.fillMaxSize(),
            shape = RoundedCornerShape(16.dp)
        ) {
            Row(
                verticalAlignment = Alignment.Top
            ) {
                BackButton(onClick = navigateBack)
                Spacer(modifier = Modifier.width(8.dp))
                Column {
                    Text(
                        text = "N° ${pokemon.id}",
                        style = MaterialTheme.typography.titleMedium.copy(),
                        color = Color.Black
                    )
                    Text(
                        text = pokemon.name,
                        style = MaterialTheme.typography.titleLarge.copy(fontWeight = FontWeight.Bold),
                        color = Color.Black
                    )
                }
//                FloatingActionButton(
//                    onClick = { onFavoriteClick() },
//                    modifier = Modifier
//                        .padding(end = 16.dp, top = 16.dp)
//                ) {
//                    Icon(
//                        imageVector = Icons.Filled.Favorite,
//                        contentDescription = "Favoris",
//                        tint = Color.Red
//                    )
//                }
            }

            Column(
                modifier = Modifier.padding(16.dp)
            ) {
                Spacer(modifier = Modifier.height(8.dp))
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                ) {

                    Spacer(modifier = Modifier.width(16.dp))
                    Column {
                        Row {
                            PokemonImage(
                                imageUrl = pokemon.image,
                                contentDescription = pokemon.name
                            )
                            PokemonTypes(types = pokemon.types)
                        }
                        Row {
                            Column {
                                PokemonStats(stats = pokemon.stats)
                            }
                            Spacer(modifier = Modifier.width(8.dp))
                            Column {
                                PokemonTypes(types = pokemon.types)
                            }
                        }
                        Spacer(modifier = Modifier.width(8.dp))
                    }
                }
                // PokemonResistances(resistances = pokemon.resistances, backgroundColor = backgroundColor)
            }
        }
    }
}

@Composable
fun PokemonImage(imageUrl: String, contentDescription: String) {
    val painter = rememberImagePainter(data = imageUrl)

    Image(
        painter = painter,
        contentDescription = contentDescription,
        modifier = Modifier.size(400.dp),
        contentScale = ContentScale.Fit
    )
}

@Composable
fun PokemonStats(stats: Stats) {
    Column {
        StatRow("PV", stats.hp, Icons.Default.Favorite, Red)
        StatRow("Attaque", stats.attack, Icons.Default.Close, Color.Black)
        StatRow("Défense", stats.defense, Icons.Default.Lock, Color.Black)
        StatRow("Vitesse", stats.speed, Icons.AutoMirrored.Filled.ArrowForward, Color.Black)
        StatRow("Attaque spéciale", stats.specialAttack, Icons.Default.Close, Color.Black)
        StatRow("Défense spéciale", stats.specialDefense, Icons.Default.Lock, Color.Black)
    }
}

@Composable
fun StatRow(label: String, value: Int, icon: ImageVector, color: Color) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(vertical = 4.dp)
    ) {
        Icon(
            imageVector = icon,
            contentDescription = label,
            tint = color,
            modifier = Modifier.size(24.dp)
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = "$label: $value",
            style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
            color = color
        )
    }
}

@Composable
fun PokemonTypes(types: List<Type>) {
    Column {
        types.forEach { type ->
            val imageUrl = type.image
            val painter: Painter = rememberImagePainter(data = imageUrl)

            Image(
                painter = painter,
                contentDescription = type.name,
                modifier = Modifier.size(90.dp),
                contentScale = ContentScale.Fit
            )
            Spacer(modifier = Modifier.width(14.dp))
        }
    }
}

@Composable
fun PokemonResistances(resistances: List<Resistance>, backgroundColor: Color) {
    Surface(
        color = backgroundColor,
        modifier = Modifier.fillMaxSize(),
        shape = RoundedCornerShape(16.dp)
    ) {

    }
    Column {
        Text(
            text = "Résistances",
            style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Bold),
            modifier = Modifier.padding(bottom = 8.dp)
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 4.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            TableCellHeader(text = "Nom")
            TableCellHeader(text = "Multiplicateur")
            TableCellHeader(text = "Relation")
        }

        resistances.forEach { resistance ->
            Row(
                modifier = Modifier.padding(vertical = 4.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                TableCell(text = resistance.name)
                TableCell(text = resistance.damageMultiplier.toString())
                TableCell(text = resistance.damageRelation)
            }
        }
    }
}

@Composable
fun TableCellHeader(text: String) {
    Text(
        text = text,
        style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
        modifier = Modifier.padding(horizontal = 4.dp)
    )
}

@Composable
fun TableCell(text: String) {
    Text(
        text = text,
        style = MaterialTheme.typography.bodyMedium,
        modifier = Modifier.padding(horizontal = 4.dp)
    )
}

@Composable
fun BackButton(onClick: () -> Unit) {
    IconButton(
        onClick = onClick,
        modifier = Modifier.padding(16.dp)
    ) {
        Icon(
            imageVector = Icons.AutoMirrored.Filled.ArrowBack,
            contentDescription = "Retour",
            tint = Color.Black
        )
    }
}
