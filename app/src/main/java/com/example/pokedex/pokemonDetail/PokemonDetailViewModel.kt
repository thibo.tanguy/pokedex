package com.example.pokedex.pokemonDetail

import android.content.Context
import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex.models.domain.Pokemon
import com.example.pokedex.services.PokemonService
import com.example.pokedex.utils.PokemonSharedPreferencesHandler
import kotlinx.coroutines.launch

class PokemonDetailViewModel : ViewModel() {
    private val pokemonService: PokemonService = PokemonService()
    private val _pokemon = mutableStateOf<Pokemon?>(null)
    private val _isLoading = mutableStateOf(false)
    private val _requestFailed = mutableStateOf(false)
    val pokemon: State<Pokemon?> = _pokemon
    val isLoading: State<Boolean> = _isLoading
    val requestFailed: State<Boolean> = _requestFailed

    fun loadPokemonById(id: Int) {
        viewModelScope.launch {
            try {
                val loadedPokemon = pokemonService.findById(id)
                _pokemon.value = loadedPokemon
                _isLoading.value = false
                _requestFailed.value = false
            }

            catch (e: Exception) {
                Log.e("error", "Request error : ", e)
                _requestFailed.value = true
                _isLoading.value = false
            }
        }
    }

    fun addPokemonToFavorites(context: Context) {
        pokemon.value?.let { PokemonSharedPreferencesHandler.savePokemon(context, it) }
    }
}
