package com.example.pokedex.services

import android.util.Log
import com.example.pokedex.models.domain.Pokemon
import com.example.pokedex.retrofit.ApiClient

class PokemonService {

    suspend fun findAll(): List<Pokemon> {
        try {
            return ApiClient.pokemonApiService.findAll().map { it.toDomain() }
        }

        catch (e: Exception) {
            Log.e("PokemonService", "Error fetching pokemons", e)
            throw e
        }
    }

    suspend fun findAllFromFirstGeneration(): List<Pokemon> {
        try {
            return ApiClient.pokemonApiService.findAllFromFirstGeneration().map { it.toDomain() }
        }

        catch (e: Exception) {
            Log.e("PokemonService", "Error fetching pokemons", e)
            throw e
        }
    }

    suspend fun findById(id: Int): Pokemon? {
        try {
            return ApiClient.pokemonApiService.findById(id)?.toDomain()
        }

        catch (e: Exception) {
            Log.e("PokemonService", "Error fetching pokemon", e)
            throw e
        }
    }
}
