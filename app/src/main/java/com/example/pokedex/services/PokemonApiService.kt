package com.example.pokedex.services

import com.example.pokedex.models.api.PokemonAPI
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonApiService {
    @GET("api/v1/pokemon")
    suspend fun findAll(): List<PokemonAPI>

    @GET("api/v1/pokemon/limit/151")
    suspend fun findAllFromFirstGeneration(): List<PokemonAPI>

    @GET("api/v1/pokemon/{id}")
    suspend fun findById(@Path("id") id: Int): PokemonAPI?
}
