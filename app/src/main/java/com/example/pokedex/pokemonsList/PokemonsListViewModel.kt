package com.example.pokedex.pokemonsList

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.ViewModel
import com.example.pokedex.models.domain.Pokemon
import com.example.pokedex.services.PokemonService
import kotlinx.coroutines.launch

class PokemonsListViewModel : ViewModel() {
    private val pokemonService: PokemonService = PokemonService()
    private val _pokemons = mutableStateOf<List<Pokemon>>(emptyList())
    private val _isLoading = mutableStateOf(false)
    private val _requestFailed = mutableStateOf(false)
    val pokemons: State<List<Pokemon>> = _pokemons
    val isLoading: State<Boolean> = _isLoading
    val requestFailed: State<Boolean> = _requestFailed

    init {
        loadPokemons()
    }

    fun loadPokemons() {
        _isLoading.value = true
        viewModelScope.launch {
            try {
                val loadedPokemons = pokemonService.findAllFromFirstGeneration()
                _pokemons.value = loadedPokemons
                _isLoading.value = false
                _requestFailed.value = false
            } catch (e: Exception) {
                Log.e("error", "Request error : ", e)
                _requestFailed.value = true
                _isLoading.value = false
            }
        }
    }
}