package com.example.pokedex.pokemonsList

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.pokedex.models.domain.Pokemon
import com.example.pokedex.models.domain.Stats
import com.example.pokedex.models.domain.Type
import com.example.pokedex.ui.theme.Red
import com.example.pokedex.ui.theme.getTypeColor

object PokemonsList {
    lateinit var viewModel: PokemonsListViewModel
    lateinit var routeBase: String

    @Composable
    fun Screen(
        navigateToPokemonDetail: (Int) -> Unit,
        viewModel: PokemonsListViewModel
    ) {
        val pokemons = viewModel.pokemons.value
        val isLoading = viewModel.isLoading.value
        val requestFailed = viewModel.requestFailed.value
        var searchText by remember { mutableStateOf("") }

        if (isLoading) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator(
                    color = Red
                )
            }
        }

        else {
            Column(modifier = Modifier.fillMaxSize()) {
                TextField(
                    value = searchText,
                    onValueChange = { searchText = it },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    label = { Text("Rechercher un pokemon ...") }
                )

                if (requestFailed) {
                    PokemonThemedButton(
                        onClick = { viewModel.loadPokemons() },
                    )
                }

                LazyColumn {
                    val filteredPokemons = pokemons.filter {
                        it.name.contains(searchText, ignoreCase = true)
                    }
                    items(filteredPokemons.size) { index ->
                        PokemonItem(
                            pokemon = filteredPokemons[index],
                            navigateToPokemonDetail = navigateToPokemonDetail
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun PokemonThemedButton(
    onClick: () -> Unit,
    enabled: Boolean = true
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(16.dp)
    ) {
        Icon(
            imageVector = Icons.Default.Warning,
            contentDescription = "Warning Icon",
            tint = Red,
            modifier = Modifier.size(48.dp)
        )
        Text(
            text = "Echec de la connexion ...",
            style = MaterialTheme.typography.bodyMedium,
            color = Color.Black,
            modifier = Modifier.padding(top = 8.dp)
        )
        Button(
            onClick = onClick,
            enabled = enabled,
            modifier = Modifier
                .padding(top = 16.dp)
                .fillMaxWidth()
        ) {
            Text(text = "Réessayer")
        }
    }
}

@Composable
fun PokemonItem(
    pokemon: Pokemon,
    navigateToPokemonDetail: (Int) -> Unit
) {
    val backgroundColor = getTypeColor(pokemon.types.last().name)

    Card(
        shape = RoundedCornerShape(16.dp),
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
            .clickable { navigateToPokemonDetail(pokemon.id) },
    ) {
        Surface(
            color = backgroundColor,
            modifier = Modifier.fillMaxSize(),
            shape = RoundedCornerShape(16.dp)
        ) {
            Column(
                modifier = Modifier.padding(16.dp)
            ) {
                Spacer(modifier = Modifier.height(8.dp))
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Column {
                        Text(
                            text = "N° ${pokemon.id}",
                            style = MaterialTheme.typography.titleMedium.copy(fontWeight = FontWeight.Bold),
                            color = Color.Black
                        )
                        PokemonImage(
                            imageUrl = pokemon.image,
                            contentDescription = pokemon.name
                        )
                    }
                    Spacer(modifier = Modifier.width(16.dp))
                    Column {
                        Text(
                            text = pokemon.name,
                            style = MaterialTheme.typography.titleLarge.copy(fontWeight = FontWeight.Bold),
                            color = Color.Black
                        )
                        PokemonStats(stats = pokemon.stats)
                        Spacer(modifier = Modifier.width(8.dp))
                        PokemonTypes(types = pokemon.types)
                    }
                }
            }
        }
    }
}

@Composable
fun PokemonImage(imageUrl: String, contentDescription: String) {
    val painter = rememberImagePainter(data = imageUrl)

    Image(
        painter = painter,
        contentDescription = contentDescription,
        modifier = Modifier.size(170.dp),
        contentScale = ContentScale.Fit
    )
}

@Composable
fun PokemonStats(stats: Stats) {
    Column {
        StatRow("PV", stats.hp, Icons.Default.Favorite, Red)
        StatRow("Attaque", stats.attack, Icons.Default.Close, Color.Black)
        StatRow("Défense", stats.defense, Icons.Default.Lock, Color.Black)
        StatRow("Vitesse", stats.speed, Icons.AutoMirrored.Filled.ArrowForward, Color.Black)
    }
}

@Composable
fun StatRow(label: String, value: Int, icon: ImageVector, color: Color) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(vertical = 4.dp)
    ) {
        Icon(
            imageVector = icon,
            contentDescription = label,
            tint = color,
            modifier = Modifier.size(24.dp)
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = "$label: $value",
            style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
            color = color
        )
    }
}

@Composable
fun PokemonTypes(types: List<Type>) {
    Row {
        types.forEach { type ->
            val imageUrl = type.image
            val painter: Painter = rememberImagePainter(data = imageUrl)

            Image(
                painter = painter,
                contentDescription = type.name,
                modifier = Modifier.size(35.dp),
                contentScale = ContentScale.Fit
            )
            Spacer(modifier = Modifier.width(4.dp))
        }
    }
}
