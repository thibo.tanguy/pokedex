package com.example.pokedex.ui.theme

import androidx.compose.ui.graphics.Color
import java.util.Locale

val Red = Color(0xFFFF0000)
val White = Color(0xFFFFFFFF)
val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)
val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val Normal = Color(0xFFA8A77A)
val Fire = Color(0xFFE79252)
val Water = Color(0xFF7095E4)
val Electric = Color(0xFFFFD147)
val Grass = Color(0xFF93BB66)
val Ice = Color(0xFF96D9D6)
val Fighting = Color(0xFF81442D)
val Poison = Color(0xFFA265A0)
val Ground = Color(0xFFE2BF65)
val Flying = Color(0xFFA98FF3)
val Psychic = Color(0xFFBB7389)
val Bug = Color(0xFF8E9B46)
val Rock = Color(0xFFA0882F)
val Ghost = Color(0xFF735797)
val Dragon = Color(0xFF7E66B9)
val Dark = Color(0xFF705746)
val Steel = Color(0xFFB7B7CE)
val Fairy = Color(0xFFD58FB1)

val typeColors: Map<String, Color> = mapOf(
    "normal" to Normal,
    "combat" to Fighting,
    "vol" to Flying,
    "poison" to Poison,
    "sol" to Ground,
    "roche" to Rock,
    "insecte" to Bug,
    "spectre" to Ghost,
    "acier" to Steel,
    "feu" to Fire,
    "eau" to Water,
    "plante" to Grass,
    "électrik" to Electric,
    "psy" to Psychic,
    "glace" to Ice,
    "dragon" to Dragon,
    "ténèbres" to Dark,
    "fée" to Fairy
)

fun getTypeColor(typeName: String): Color {
    return typeColors[typeName.lowercase(Locale.ROOT)] ?: White
}