package com.example.pokedex

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.pokedex.pokemonDetail.PokemonDetail
import com.example.pokedex.pokemonDetail.PokemonDetail.navigateToPokemonDetail
import com.example.pokedex.pokemonDetail.PokemonDetail.pokemonDetailNavigationEntry
import com.example.pokedex.pokemonsFavouritesList.PokemonsFavouritesList
import com.example.pokedex.pokemonsFavouritesList.PokemonsFavouritesList.navigateToPokemonsFavourites
import com.example.pokedex.pokemonsList.PokemonsList

@Composable
fun MainNavGraph(context: Context) {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = PokemonsList.routeBase
    ) {

        composable(PokemonsList.routeBase) {
            PokemonsList.Screen(
                navigateToPokemonDetail = { navController.navigateToPokemonDetail(it) },
                viewModel = PokemonsList.viewModel
            )
        }

//        composable(PokemonsFavouritesList.routeBase) {
//            PokemonsFavouritesList.Screen(
//                navigateToPokemonDetail = { navController.navigateToPokemonsFavourites(it) },
//                viewModel = PokemonsList.viewModel
//            )
//        }

        pokemonDetailNavigationEntry(
            navigateBack = { navController.popBackStack() },
            viewModel = PokemonDetail.viewModel,
            onFavouriteClick = {
                // Logique pour ajouter le Pokémon aux favoris
                // Par exemple :
                PokemonDetail.viewModel.addPokemonToFavorites(context)
            }
        )
//
//        pokemonsFavouritesNavigationEntry(
//            navigateBack = { navController.popBackStack() },
//            viewModel = PokemonsFavouritesList.viewModel,
//        )
    }
}
